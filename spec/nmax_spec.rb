RSpec.describe Nmax do
  let(:file_path) { "spec/fixtures/file/sample" }
  let(:file) { File.open(file_path) }

  it "has a version number" do
    expect(described_class::VERSION).not_to be(nil)
  end

  describe ".call" do
    context "when numbers count" do
      context "is missing" do
        it "throws error" do
          expect{ described_class.call }.to raise_error(ArgumentError)
        end
      end

      context "is invalid" do
        it "throws error" do
          call = described_class.call("invalid", file)

          expect{ call }.not_to raise_error
          expect(call).to eq("Invalid numbers count. Must be integer")
        end
      end
    end

    context "when data" do
      context "is missing" do
        it "throws error" do
          expect{ described_class.call(5) }.to raise_error(ArgumentError)
        end
      end

      context "is invalid" do
        it "throws error" do
          call = described_class.call(5, "invalid")

          expect{ call }.not_to raise_error
          expect(call).to eq("Invalid data")
        end
      end
    end

    it "returns max numbers" do
      expect(described_class.call(5, file)).to eq([500, 100, 99, 67, 40])
    end
  end

  describe "exe/nmax" do
    it "returns max numbers" do
      command = `cat #{file_path} | exe/nmax 3`

      expect(command).to eq("500\n100\n99\n")
    end
  end
end
