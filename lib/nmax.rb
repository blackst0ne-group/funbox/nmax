require "nmax/version"

module Nmax
  def self.call(numbers_count, data)
    numbers_count = Integer(numbers_count)

    # Performance can be increased by merging `to_i`` and sorting (finding maximum) to the `scan` loop.
    data.read.scan(/\d{1,1000}/).map(&:to_i).max(numbers_count)
  rescue ArgumentError
    "Invalid numbers count. Must be integer"
  rescue NoMethodError
    "Invalid data"
  end
end
